

Dear SitePoint Member,

We have recently confirmed that SitePoint’s infrastructure was breached by a third party and some non-sensitive customer data was accessed as part of this attack.  

As a precautionary measure, while we continue to investigate, we have reset passwords on all accounts and increased our required length to 10 characters. Next time you login to SitePoint you will need to create a new password. 

Your browser will remain logged in if you have used our service recently. However, you can still create a new password manually by clicking on the ‘Account > Profile & Settings’ option and entering your details in the ‘Change your password’ section. 

If you use Social Login (e.g. Google or Facebook), you will be able to login as normal. 

If you have deactivated your SitePoint account, no action is required however we recommend you refer to the 'What can I do to protect myself?' section.

__What information does this relate to?__

At this point, we believe the accessed information mainly relates to your name, email address, hashed password, username, and IP address.

__Did they get access to my Password?__

All passwords are uniquely hashed and salted for security purposes and therefore much harder for malicious parties to access. Still, we recommend you update your SitePoint password.

__Did they access any financial or Credit Card information?__

No. There is currently no evidence your financial information was accessed at this stage. We do not store your Credit Card information in our system, we use a third party service (Stripe) for all credit card processing. 

__What can I do to protect myself?__

We recommend that you change passwords from any other websites that may be a duplicate of your SitePoint password, just as a precaution. 

__How did this happen?__

Investigations suggest this attack was a result of a third party tool we used to monitor our GitHub account, which was compromised by malicious parties. This allowed access through our codebase into our systems. This tool has since been removed, all of our API keys rotated and passwords changed. 

__What will happen next?__

We are currently performing a full assessment of the data breach, and our infrastructure, and security. You will be notified of any additional changes or risks if they arise.

We are very sorry for any inconvenience this has caused.  Please contact us at security@sitepoint.com if you have any further questions or concerns. 

As always we appreciate your trust and support.

Thank you,

SitePoint Team
